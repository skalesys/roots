GOMPLATE=$(ROOTS_VENDOR_PATH)/gomplate
ROOTS_LIBRARY_GOMPLATE_PATH=$(ROOTS_LIBRARY_PATH)/gomplate

# Contributing template, if none is provided
export CONTRIBUTING ?= https://gitlab.com/skalesys/docs/raw/master/CONTRIBUTING.tmpl

# Commercial support template, if none is provided
export COMMERCIAL ?= https://gitlab.com/skalesys/docs/raw/master/COMMERCIAL.tmpl

# Set your company's name at your main Makefile
export COMPANY_NAME ?= SkaleSys

# Set your company's websitename at your main Makefile
export COMPANY_WEBSITE ?= https://skalesys.com

export README_YAML ?= docs/README.yaml
export README_INCLUDES ?= $(file://$(shell pwd)/?type=text/plain)

# Set your project's logo theme, will download a new one from unsplash
export LOGO_THEME ?= "nature,mountain"

.PHONY: gomplate/install
## Install Gomplate
gomplate/install:
	@( \
		source $(ROOTS_VENV_PATH)/bin/activate && \
		ansible-playbook -v $(ROOTS_LIBRARY_ANSIBLE_PLAYBOOKS_PATH)/gomplate.yml --extra-vars "gomplate_install_dir=$(ROOTS_VENDOR_PATH)" \
	)

.PHONY: gomplate/version
## Displays Gomplate version
gomplate/version:
	@$(GOMPLATE) --version


.PHONY: readme/install
## Install README
readme/install: gomplate/install
	@( \
		source $(ROOTS_VENV_PATH)/bin/activate && \
		ansible-playbook -v $(ROOTS_LIBRARY_ANSIBLE_PLAYBOOKS_PATH)/readme.yml --extra-vars "project_docs_path=$(PROJECT_DOCS_PATH) roots_user=$(USER) logo_theme=$(LOGO_THEME)" \
	)

.PHONY: readme/targets
readme/targets:
	@$(SELF) --silent help/all/plain > docs/targets.md

## Alias for readme/build
readme: readme/build
	@exit 0

## Create README.md by building it from README.yaml
readme/build: readme/install readme/targets
		@$(GOMPLATE) --file $(ROOTS_LIBRARY_GOMPLATE_PATH)/datasource/README.md --out $(PROJECT_PATH)/README.md
		$(call pretty-message,README.md generated sucessfully!)
