export ROOTS_LIBRARY_TERRAFORM_PATH = $(ROOTS_LIBRARY_PATH)/terraform


# You can override the Terraform version at the main Makefile at your project,e.g.:
# export TERRAFORM_VERSION ?= 0.11.11
TERRAFORM_VERSION ?= 0.11.14

# Terraform default environment
ENVIRONMENT ?=
TERRAFORM ?= $(ROOTS_VENDOR_PATH)/terraform

TERRAFORM_PLAN ?= terraform.plan
TERRAFORM_VARS ?= environments/$(ENVIRONMENT).tfvars
TERRAFORM_VENDOR = $(ROOTS_VENDOR_PATH)/terraform_$(TERRAFORM_VERSION)

# AWS REGION
REGION ?=

# S3 BACKEND DEFAULT TO ALL PROJECTS!
BUCKET ?=
DYNAMODB_TABLE ?=

.PHONY: terraform/apply
## Builds or changes infrastructure
terraform/apply: terraform/plan
	@(cd terraform && $(TERRAFORM) apply -auto-approve -var-file=$(TERRAFORM_VARS))

.PHONY: terraform/clean
## Cleans Terraform vendor from Maker
terraform/clean:
	rm -rf $(ROOTS_VENDOR_PATH)/terraform
	rm -rf $(TERRAFORM_VENDOR)
	rm -rf $(PROJECT_PATH)/terraform/.terraform/
	rm -f $(PROJECT_PATH)/terraform/terraform.*

.PHONY: terraform/console
## Interactive console for Terraform interpolations
terraform/console:
	@$(TERRAFORM) console

.PHONY: terraform/destroy
## Destroy Terraform-managed infrastructure, removes .terraform and local state files
terraform/destroy: terraform/fmt terraform/validate
	$(call assert-set,ENVIRONMENT)
	@(cd terraform && $(TERRAFORM) destroy -auto-approve -var-file=$(TERRAFORM_VARS))

.PHONY: terraform/fmt
## Rewrites config files to canonical format
terraform/fmt:
	@$(TERRAFORM) fmt

.PHONY: terraform/get
## Download and install modules for the configuration
terraform/get:
	(cd terraform && $(TERRAFORM) get)

.PHONY: terraform/graph
## Create a visual graph of Terraform resources
terraform/graph:
	@$(TERRAFORM) graph

.PHONY: terraform/init
## Initialize a Terraform working directory
terraform/init:
	@( cd terraform && $(TERRAFORM) init)

.PHONY: terraform/init-s3-backend
## Initialize a Terraform working directory with S3 as backend and DynamoDB for locking
terraform/init-s3-backend:
	$(call assert-set,BUCKET)
	$(call assert-set,REGION)
	$(call assert-set,PROJECT_NAME)
	$(call assert-set,ENVIRONMENT)
	$(call assert-set,DYNAMODB_TABLE)
	@$(TERRAFORM) init -backend-config="bucket=$(BUCKET)" \
	-backend-config="region=$(REGION)" \
	-backend-config="key=$(PROJECT_NAME)/$(ENVIRONMENT)/terraform.state" \
	-backend-config="encrypt=true" \
	-backend-config="dynamodb_table=$(DYNAMODB_TABLE)"

.PHONY: terraform/install
## Install terraform
terraform/install:
	@( \
		source $(ROOTS_VENV_PATH)/bin/activate && \
		ansible-playbook -v $(ROOTS_LIBRARY_ANSIBLE_PLAYBOOKS_PATH)/terraform.yml --extra-vars "terraform_bin_path=$(ROOTS_VENDOR_PATH) terraform_version=$(TERRAFORM_VERSION)" --tags "install" \
	)

.PHONY: terraform/output
## Read an output from a state file
terraform/output:
	@(cd terraform && $(TERRAFORM) output -json)

.PHONY: terraform/plan
## Generate and show an execution plan
terraform/plan: terraform/fmt terraform/validate
	$(call assert-set,ENVIRONMENT)
	@(cd terraform && $(TERRAFORM) plan -out=$(TERRAFORM_PLAN) -var-file=$(TERRAFORM_VARS))

.PHONY: terraform/providers
## Prints a tree of the providers used in the configuration
terraform/providers:
	@$(TERRAFORM) providers

.PHONY: terraform/push
## Upload this Terraform module to Atlas to run
terraform/push:
	@$(TERRAFORM) push

.PHONY: terraform/refresh
## Update local state file against real resources
terraform/refresh:
	@$(TERRAFORM) refresh

.PHONY: terraform/show
## Inspect Terraform state or plan
terraform/show:
	@$(TERRAFORM) show

.PHONY: terraform/taint
## Manually mark a resource for recreation
terraform/taint:
	@$(TERRAFORM) taint

.PHONY: terraform/template
terraform/template:
	@( \
		source $(ROOTS_VENV_PATH)/bin/activate && \
		ansible-playbook -v $(ROOTS_LIBRARY_ANSIBLE_PLAYBOOKS_PATH)/terraform.yml --extra-vars "project_path=$(PROJECT_PATH) terraform_version=$(TERRAFORM_VERSION)" --tags "template" \
	)

.PHONY: terraform/upgrade
terraform/upgrade:
	@(cd terraform && $(TERRAFORM) 0.12upgrade -var-file=$(TERRAFORM_VARS))

	@$(TERRAFORM) 0.12upgrade

.PHONY: terraform/untaint
## Manually unmark a resource as tainted
terraform/untaint:
	@$(TERRAFORM) untaint

.PHONY: terraform/validate
## Validates the Terraform files
terraform/validate:
	$(call assert-set,ENVIRONMENT)
	@(cd terraform && $(TERRAFORM) validate -var-file=$(TERRAFORM_VARS))

.PHONY: terraform/version
## Prints the Terraform version
terraform/version:
	@$(TERRAFORM) version

.PHONY: terraform/workspace
## Select workspace
terraform/workspace:
	$(call assert-set,ENVIRONMENT)
	$(call pretty-message,Terraform worksapce: $(ENVIRONMENT))
	@$(TERRAFORM) workspace select $(ENVIRONMENT)
