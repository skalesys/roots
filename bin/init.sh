#!/usr/bin/env bash

# Purpose: Allow development and testing of the project
# Usage ./bin/init.sh

rm -rf ../builder
mkdir -p ../builder/.roots && cp -r ./ ../builder/.roots
cd ../builder

if [[ ! -f Makefile ]]; then
    echo 'SHELL = /bin/sh' > Makefile

    # if you wanna test against the local code
    echo '-include .roots/Makefile' >> Makefile
fi

make init