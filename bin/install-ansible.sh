#!/usr/bin/env bash

echo "============="
echo "UPDATE SYSTEM"
echo "============="
sudo apt-get -qq update

echo "===================="
echo "INSTALL REQUIREMENTS"
echo "===================="
sudo apt-get -qq install --yes software-properties-common

if ! grep -q "^deb .*ansible" /etc/apt/sources.list /etc/apt/sources.list.d/*; then
    echo "=========================="
    echo "ADD ANSIBLE PPA REPOSITORY"
    echo "=========================="
    sudo apt-add-repository --yes --update ppa:ansible/ansible
fi

echo "==============="
echo "INSTALL ANSIBLE"
echo "==============="
sudo apt-get -qq install --yes ansible
ansible --version